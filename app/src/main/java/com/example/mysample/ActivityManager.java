package com.example.mysample;

import android.content.Context;
import android.content.Intent;

public class ActivityManager {

    public static void startActivity(Context context, Class activityClass) {
        Intent intent = new Intent(context, activityClass);
        context.startActivity(intent);
    }
}
//        PdfActivity.showDocument(this, Uri.parse(), configuration);
