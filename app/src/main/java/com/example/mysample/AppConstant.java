package com.example.mysample;

public class AppConstant {
    public static final String BASE_URL = "http://192.168.122.5/teambindermobiletb5dev/";
    public static final String NO_NETWORK_CONNECTION = "No network connection found...";
    public static final String LIST_NO_OF_ROWS = "50";

    public class SharedPrefKeys {
        public static final String APP_PREFERENCES = "appPreferences";
        public static final String KEY_IS_LOGGED_IN = "isLoggedIn";
        public static final String KEY_SESSION_ID = "sessionId";
    }
}
