package com.example.mysample;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.mysample.dialog.CommonDialogFragment;

public class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();
    private static final String TAG_COMMON_DIALOG = "commonDialog";
    public Dialog mProgress;

    protected Fragment startFragment (int id, Fragment fragment, boolean isBackStackEnable, String key){
        FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction();
        ft.replace(id , fragment, key);
        if (isBackStackEnable)
            ft.addToBackStack(null);
        ft.commit();
        return fragment;
    }


    protected boolean checkInstance(String key){
        if (getSupportFragmentManager().findFragmentByTag(key) != null && getSupportFragmentManager().findFragmentByTag(key).isVisible()) {
            Log.d(TAG, "checkInstance - fragment already an instance " + key);
            return true;
        }
        Log.d(TAG, "checkInstance ----- out of stack");
        return false;
    }

    //progress view
    protected void showWaiting(Context context){
        if(mProgress == null){
            mProgress = new Dialog(context, R.style.Progressbar);
            mProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mProgress.setContentView(R.layout.custom_progress);
            mProgress.setCancelable(false);
        }

        if(!mProgress.isShowing()){
            mProgress.show();
        }
    }

    //dismiss progress view
    protected void dismissWaiting() {
        if(mProgress != null && mProgress.isShowing()){
            mProgress.dismiss();
            mProgress = null;
        }
    }

    protected static void showCustomDialog(FragmentActivity context, String dialogText, boolean isSingleButton, String buttonText) {
        FragmentTransaction fragmentTransaction = context.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        CommonDialogFragment commonDialogFragment = CommonDialogFragment.newInstance(dialogText, isSingleButton, buttonText);
        if(!commonDialogFragment.isVisible()){
            commonDialogFragment.show(fragmentTransaction, TAG_COMMON_DIALOG);
        }
    }
}
