package com.example.mysample.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.mysample.object.User;

import java.util.ArrayList;
import java.util.List;

public class ContactsDataSource {

    private static final String TAG = ContactsDataSource.class.getSimpleName();
    private String[] allColumns = {DatabaseConstants.USER_ID, DatabaseConstants.USER_COMPANY_ID, DatabaseConstants.FIRST_NAME
            ,DatabaseConstants.LAST_NAME, DatabaseConstants.COMPANY_NAME, DatabaseConstants.EMAIL_ADDRESS
            ,DatabaseConstants.ADDRESS, DatabaseConstants.MOBILE_NUMBER, DatabaseConstants.USERNAME
            ,DatabaseConstants.IS_ACTIVE, DatabaseConstants.IS_OFFLINE_DATA};
    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;

    public ContactsDataSource(Context context) {
        this.dbHelper = new DatabaseHelper(context);
    }

    public void open() {
        this.database = this.dbHelper.getWritableDatabase();
    }

    public void close() {
        this.dbHelper.close();
    }

    public User createContact(User user){
        Log.d(TAG, "createContact: " + user.toString());
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseConstants.USER_COMPANY_ID, user.getUniqueId());
        contentValues.put(DatabaseConstants.FIRST_NAME, user.getFirstName());
        contentValues.put(DatabaseConstants.LAST_NAME, user.getLastName());
        contentValues.put(DatabaseConstants.COMPANY_NAME, user.getCompanyName());
        contentValues.put(DatabaseConstants.EMAIL_ADDRESS, user.getEmailAddress());
        contentValues.put(DatabaseConstants.ADDRESS, user.getAddress());
        contentValues.put(DatabaseConstants.MOBILE_NUMBER, user.getMobileNumber());
        contentValues.put(DatabaseConstants.USERNAME, user.getUsername());
        contentValues.put(DatabaseConstants.IS_ACTIVE, user.isActive());
        contentValues.put(DatabaseConstants.IS_OFFLINE_DATA, user.isOfflineData());
        long insertId = this.database.insert(DatabaseConstants.TABLE_CONTACTS, null, contentValues);
        Cursor cursor = this.database.query(DatabaseConstants.TABLE_CONTACTS, this.allColumns
                , DatabaseConstants.USER_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        User newUser = cursorToUsers(cursor);
        cursor.close();
        Log.d(TAG, "INSERTED USER : " + insertId);
        return newUser;
    }

    public User updateContact(User user) {
        Log.d(TAG, "updateUser: " + user.toString());
        User updateUser = new User();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseConstants.USER_COMPANY_ID, user.getUniqueId());
        contentValues.put(DatabaseConstants.FIRST_NAME, user.getFirstName());
        contentValues.put(DatabaseConstants.LAST_NAME, user.getLastName());
        contentValues.put(DatabaseConstants.COMPANY_NAME, user.getCompanyName());
        contentValues.put(DatabaseConstants.EMAIL_ADDRESS, user.getEmailAddress());
        contentValues.put(DatabaseConstants.ADDRESS, user.getAddress());
        contentValues.put(DatabaseConstants.MOBILE_NUMBER, user.getMobileNumber());
        contentValues.put(DatabaseConstants.USERNAME, user.getUsername());
        contentValues.put(DatabaseConstants.IS_ACTIVE, user.isActive());
        contentValues.put(DatabaseConstants.IS_OFFLINE_DATA, user.isOfflineData());
        this.database.update(DatabaseConstants.TABLE_CONTACTS, contentValues
                , DatabaseConstants.USER_ID + " = " + user.getUserId(), null);
        Log.d(TAG, "UPDATED USER : " + user.getUserId());
        return updateUser;
    }

    public void deleteContact(User user) {
        Log.d(TAG, "deleteContact: " + user.toString());
        this.database.delete(DatabaseConstants.TABLE_CONTACTS, DatabaseConstants.USER_ID + " =? "
                , new String[]{Long.toString(user.getUserId())});
        Log.d(TAG, "DELETED USER : " + user.getUserId());
    }

    public List<User> getContactList() {
        Log.d(TAG, "getContactList: ");
        List<User> userList = new ArrayList<>();
        Cursor cursor = this.database.query(DatabaseConstants.TABLE_CONTACTS, this.allColumns
                , null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            userList.add(cursorToUsers(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        Log.d(TAG, "NUMBER OF CONTACTS : " + userList.size());
        return userList;
    }

    private User cursorToUsers(Cursor cursor) {
        Log.d(TAG, "cursorToUsers: " +cursor.toString());
        User users = new User();
        users.setUserId(cursor.getLong(0));
        users.setUniqueId(cursor.getString(1));
        users.setFirstName(cursor.getString(2));
        users.setLastName(cursor.getString(3));
        users.setCompanyName(cursor.getString(4));
        users.setEmailAddress(cursor.getString(5));
        users.setAddress(cursor.getString(6));
        users.setMobileNumber(cursor.getString(7));
        users.setUsername(cursor.getString(8));
        users.setActive(cursor.getInt(9) > 0);
        users.setIsOfflineData(cursor.getInt(10) > 0);
        return users;
    }
}
