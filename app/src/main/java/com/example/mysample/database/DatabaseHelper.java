package com.example.mysample.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String TAG = DatabaseHelper.class.getSimpleName();

    public DatabaseHelper(Context context) {
        super(context, DatabaseConstants.DATABASE_NAME, null, DatabaseConstants.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DatabaseConstants.TABLE_CREATE_CONTACTS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldDatabaseVersion, int newDatabaseVersion) {
        Log.d(TAG, "onUpgrade: old version--> " + oldDatabaseVersion
                + " new version --> " + newDatabaseVersion + ", deleting all data");
        sqLiteDatabase.execSQL(DatabaseConstants.DROP_TABLE_CONTACTS);
        onCreate(sqLiteDatabase);
    }
}
