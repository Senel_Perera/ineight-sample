package com.example.mysample.database;

public class DatabaseConstants {
    //database constants
    public static final String DATABASE_NAME = "sample.db";
    public static final int DATABASE_VERSION = 1;

    //contacts table constants
    public static final String TABLE_CONTACTS = "contacts";
    public static final String DROP_TABLE_CONTACTS = "DROP TABLE IF EXISTS contacts";
    public static final String TABLE_CREATE_CONTACTS = "create table " + TABLE_CONTACTS + "(user_id integer primary key autoincrement" +
            ", user_company_id text not null unique, first_name text not null, last_name text not null, company_name text not null, email_address text not null" +
            ", address text not null, mobile_number text not null, username text not null, is_active bit, is_offline_data bit);";
    public static final String USER_ID = "user_id";
    public static final String USER_COMPANY_ID = "user_company_id";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String COMPANY_NAME = "company_name";
    public static final String EMAIL_ADDRESS = "email_address";
    public static final String ADDRESS = "address";
    public static final String MOBILE_NUMBER = "mobile_number";
    public static final String USERNAME = "username";
    public static final String IS_ACTIVE = "is_active";
    public static final String IS_OFFLINE_DATA = "is_offline_data";
}
