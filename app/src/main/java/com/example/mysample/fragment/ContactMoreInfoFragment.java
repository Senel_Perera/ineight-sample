package com.example.mysample.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;

import com.example.mysample.BaseFragment;
import com.example.mysample.R;
import com.example.mysample.helper.Helper;
import com.example.mysample.helper.Validator;
import com.example.mysample.object.User;
import com.example.mysample.widget.CustomFormRow;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ContactMoreInfoFragment extends BaseFragment {

    private static final String TAG = ContactMoreInfoFragment.class.getSimpleName();
    private static final String KEY_CONTACT = "key_contact";
    private List<CustomFormRow> customFormRows = new ArrayList<>();
    private boolean isEditMode = false;
    private User mUser;

    @BindView(R.id.formCompanyName)
    CustomFormRow formCompanyName;
    @BindView(R.id.formDepartment)
    CustomFormRow formDepartment;
    @BindView(R.id.formType)
    CustomFormRow formType;
    @BindView(R.id.formFirstName)
    CustomFormRow formFirstName;
    @BindView(R.id.formLastName)
    CustomFormRow formLastName;
    @BindView(R.id.formContactId)
    CustomFormRow formContactId;
    @BindView(R.id.formTitle)
    CustomFormRow formTitle;
    @BindView(R.id.formPosition)
    CustomFormRow formPosition;
    @BindView(R.id.formPhone)
    CustomFormRow formPhone;
    @BindView(R.id.formMobile)
    CustomFormRow formMobile;
    @BindView(R.id.formFax)
    CustomFormRow formFax;
    @BindView(R.id.formEmail)
    CustomFormRow formEmail;
    @BindView(R.id.formPreferredMethod)
    CustomFormRow formPreferredMethod;
    @BindView(R.id.formAddressOne)
    CustomFormRow formAddressOne;
    @BindView(R.id.formAddressTwo)
    CustomFormRow formAddressTwo;
    @BindView(R.id.formCity)
    CustomFormRow formCity;
    @BindView(R.id.formState)
    CustomFormRow formState;
    @BindView(R.id.formPostCode)
    CustomFormRow formPostCode;
    @BindView(R.id.formCountry)
    CustomFormRow formCountry;
    @BindView(R.id.formRemarks)
    CustomFormRow formRemarks;
    @BindView(R.id.swExternalContact)
    Switch swExternalContact;
    @BindView(R.id.swInActive)
    Switch swInActive;
    @BindView(R.id.swRestricted)
    Switch swRestricted;
    @BindView(R.id.swIncludeInactiveContacts)
    Switch swIncludeInactiveContacts;
    @BindView(R.id.btnSave)
    Button btnSave;
    @BindView(R.id.btnCancel)
    Button btnCancel;

    private OnFragmentInteractionListener mListener;

    public ContactMoreInfoFragment() {
        // Required empty public constructor
    }

    public static ContactMoreInfoFragment newInstance(User user) {
        ContactMoreInfoFragment fragment = new ContactMoreInfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_CONTACT, user);
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick(R.id.btnSave)
    public void onBtnSavePressed(){
        if(mListener!=null){
            setUserData();
            if(!Helper.isCustomEditsEmpty(customFormRows) && Validator.isValidPhoneNo(formMobile.getEditText())
                    && Validator.isEmail(formEmail.getEditText())){
                mListener.onSaveEditedContacts(mUser);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUser = getArguments().getParcelable(KEY_CONTACT);
        }
        Log.d(TAG, "User " + mUser.getUserId() + " " + mUser.getFirstName() + " will be displayed");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_contact_more_info, container, false);
        init(ContactMoreInfoFragment.this, view);
        addToEditTextList();
        initializeFormView();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //toggle form edit enable/disable
        editFormLogic();
        return super.onOptionsItemSelected(item);
    }

    private void setUserData() {
        mUser.setFirstName(formFirstName.getFieldText().toString());
        mUser.setLastName(formLastName.getFieldText().toString());
        mUser.setMobileNumber(formMobile.getFieldText().toString());
        mUser.setEmailAddress(formEmail.getFieldText().toString());
    }

    private void editFormLogic() {
        if(!isEditMode){
            isEditMode = true;
            btnSave.setVisibility(View.VISIBLE);
            btnCancel.setVisibility(View.VISIBLE);
            Helper.showToastMessage(getContext(), getResources().getString(R.string.edit_enabled));
        } else {
            isEditMode = false;
            btnSave.setVisibility(View.GONE);
            btnCancel.setVisibility(View.GONE);
            Helper.showToastMessage(getContext(), getResources().getString(R.string.edit_disabled));
        }
        Helper.toggleEditableFormFields(customFormRows, isEditMode);
    }

    private void initializeFormView() {
        formFirstName.setFieldText(mUser.getFirstName());
        formLastName.setFieldText(mUser.getLastName());
        formCompanyName.setFieldText(mUser.getCompanyName());
        formEmail.setFieldText(mUser.getEmailAddress());
        formAddressOne.setFieldText(mUser.getAddress());
        formMobile.setFieldText(mUser.getMobileNumber());
        formContactId.setFieldText(mUser.getUniqueId());
        swInActive.setChecked(mUser.isActive());
    }

    private void addToEditTextList() {
        customFormRows.add(formFirstName);
        customFormRows.add(formLastName);
        customFormRows.add(formPosition);
        customFormRows.add(formPhone);
        customFormRows.add(formMobile);
        customFormRows.add(formFax);
        customFormRows.add(formEmail);
        customFormRows.add(formRemarks);
        Log.d(TAG, "addToEditTextList: " + customFormRows.size());
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        void onSaveEditedContacts(User user);
    }
}
