package com.example.mysample.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mysample.BaseFragment;
import com.example.mysample.R;
import com.example.mysample.adapter.AddressAdapter;
import com.example.mysample.dialog.CommonDialogFragment;
import com.example.mysample.helper.Helper;
import com.example.mysample.helper.RecycleItemTouch;
import com.example.mysample.object.User;
import com.example.mysample.service.RequestType;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ContactsFragment extends BaseFragment implements RecycleItemTouch.RecyclerItemTouchListener {
    private static final String TAG =  ContactsFragment.class.getSimpleName();
    private static final String TAG_COMMON_DIALOG = "commonDialog";
    private OnFragmentInteractionListener mListener;
    private AddressAdapter mAddressAdapter;
    private boolean isSearchData = false;
    private String message;
    private String buttonText;
    private boolean isSingleButton;

    @BindView(R.id.etSearchContact)
    EditText etSearchContact;
    @BindView(R.id.rvContactList)
    RecyclerView rvContactList;

    public ContactsFragment() {
        // Required empty public constructor
    }

    public static ContactsFragment newInstance() {
        ContactsFragment fragment = new ContactsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private AddressAdapter.AddressAdapterCallback mAddressAdapterCallBack = new AddressAdapter.AddressAdapterCallback() {
        @Override
        public void onItemClick(int adapterPosition, View view, User user) {
            Log.d(TAG, "onItemClick: user: " + user.toString());
            if(mListener!=null){
                mListener.onContactClick(user);
            }
        }

        @Override
        public void onItemLongPress(int adapterPosition, View view, User user) {
            Log.d(TAG, "onItemLongPress: user: " + user.toString());
            Helper.showToastMessage(getActivity(), getResources().getString(R.string.long_press));
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mListener!=null){
            mListener.onApiCall(RequestType.getAllContacts, null);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);
        init(ContactsFragment.this, view);
        initializeListView();
        etSearchContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH){
                    Log.d(TAG, "onEditorAction: SEARCH '" + etSearchContact.getText().toString() +"'" );
                    mAddressAdapter.getFilter().filter(etSearchContact.getText().toString());
                    isSearchData = true;
                    return true;
                }
                return false;
            }
        });

        etSearchContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0) {
                    mAddressAdapter.reloadUserData();
                    isSearchData = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        rvContactList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(!recyclerView.canScrollVertically(1) && !isSearchData){
                    Log.d(TAG, "onScrollStateChanged: LAST POSITION" + mAddressAdapter.getItemCount());
                    if(mListener!=null){
                        mListener.onLoadMore(mAddressAdapter.getItemCount());
                    }
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        Log.d(TAG, "onSwiped: Delete confirmation " + position);
        listSwipeLogic(mAddressAdapter.getUser(position));
    }

    private void listSwipeLogic(User user) {
        if(user.isOfflineData()){
            message = getResources().getString(R.string.delete_confirmation);
            isSingleButton = false;
            buttonText = getResources().getString(R.string.no);
        } else {
            message = getResources().getString(R.string.cannot_delete);
            isSingleButton = true;
            buttonText = getResources().getString(R.string.ok);
        }
        initializeDialog(user, message, isSingleButton, buttonText);
    }

    private void initializeListView() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(ContactsFragment.this.getContext()
                , LinearLayoutManager.VERTICAL, false);
        rvContactList.setLayoutManager(layoutManager);
        ArrayList<User> users = new ArrayList<>();
        mAddressAdapter = new AddressAdapter(users, mAddressAdapterCallBack);
        rvContactList.setAdapter(mAddressAdapter);
        rvContactList.setItemAnimator(new DefaultItemAnimator());
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecycleItemTouch(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rvContactList);
    }

    private void initializeDialog(User user, String message, boolean isSingleButton, String buttonText) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        CommonDialogFragment commonDialogFragment = CommonDialogFragment.newInstance(message, isSingleButton, buttonText);
        commonDialogFragment.setListener(new CommonDialogFragment.OnCommonDialogListener() {
            @Override
            public void onYesClick() {
                mListener.onApiCall(RequestType.deleteContact, user);
                commonDialogFragment.dismiss();
            }

            @Override
            public void onNoClick() {
                mAddressAdapter.refresh();
            }
        });
        commonDialogFragment.show(fragmentTransaction, TAG_COMMON_DIALOG);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void setContacts(List<User> list, Boolean isClear) {
        mAddressAdapter.addAll(list,isClear);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        void onApiCall(RequestType requestType, Object object);
        void onContactClick(User user);
        void onLoadMore(int position);
//        void onSearch(String searchText);
    }
}
