package com.example.mysample;

import android.app.Application;

import com.example.mysample.service.Controller;

import org.greenrobot.eventbus.EventBus;

public class SampleApplication extends Application {
    private static SampleApplication mInstance;
    private Controller mController;

    public static SampleApplication getInstance(){
        if(mInstance == null){
            mInstance = new SampleApplication();
        }
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if(mController == null){
            mController = new Controller(this);
        }
        EventBus.getDefault().register(mController);
    }
}
