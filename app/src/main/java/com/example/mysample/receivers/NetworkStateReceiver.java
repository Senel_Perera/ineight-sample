package com.example.mysample.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import com.example.mysample.AppConstant;
import com.example.mysample.helper.Helper;

public class NetworkStateReceiver extends BroadcastReceiver {
    public static final String TAG = NetworkStateReceiver.class.getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: ");
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo == null){
            Log.d(TAG, "onReceive: " + AppConstant.NO_NETWORK_CONNECTION);
            return;
        }

        if(networkInfo.isConnected()) {
            switch (Helper.getNetworkType(context)) {
                case MOBILE:
                    Log.d(TAG, "NETWORK CONNECTED: MOBILE");
                    break;
                case WIFI:
                    Log.d(TAG, "NETWORK CONNECTED: WIFI");
                    break;
                case NONE:
                    Log.d(TAG, "NETWORK CONNECTED: NONE");
                    break;
            }
        } else {
            Log.d(TAG, "onReceive: Network disconnected...");
        }
    }
}
