package com.example.mysample.dialog;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.mysample.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CommonDialogFragment extends DialogFragment {

    private static final String TAG = CommonDialogFragment.class.getSimpleName();
    private static final String KEY_DIALOG_TEXT = "key_dialog_text";
    private static final String KEY_SINGLE_BUTTON = "key_single_button";
    private static final String KEY_BUTTON_TEXT = "key_button_text";
    private OnCommonDialogListener mListener;
    private String dialogMessage;
    private boolean isSingleButton;
    private String buttonText;
    protected Unbinder unbinder;

    @BindView(R.id.btnYes)
    Button btnYes;
    @BindView(R.id.btnNo)
    Button btnNo;
    @BindView(R.id.tvDialogText)
    TextView tvDialogText;

    public CommonDialogFragment() {

    }

    public void setListener(OnCommonDialogListener mListener) {
        this.mListener = mListener;
    }

    public static CommonDialogFragment newInstance(String message, boolean isSingleButton, String buttonText) {
        CommonDialogFragment fragment = new CommonDialogFragment();
        Bundle args = new Bundle();
        args.putString(KEY_DIALOG_TEXT, message);
        args.putString(KEY_BUTTON_TEXT, buttonText);
        args.putBoolean(KEY_SINGLE_BUTTON, isSingleButton);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dialogMessage = getArguments().getString(KEY_DIALOG_TEXT);
            isSingleButton = getArguments().getBoolean(KEY_SINGLE_BUTTON);
            buttonText = getArguments().getString(KEY_BUTTON_TEXT);
            Log.d(TAG, "onCreate: " + dialogMessage + "SINGLE BUTTON: " + isSingleButton + "BUTTON TEXT: " + buttonText);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_common_dialog, container, false);
        unbinder = ButterKnife.bind(this, view);
        initialize();
        return view;
    }

    private void initialize() {
        if (dialogMessage != null){
            tvDialogText.setText(dialogMessage);
        }

        if (isSingleButton){
            btnYes.setVisibility(View.GONE);
        }

        if (buttonText != null){
            btnNo.setText(buttonText);
        }

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: yes");
                if(mListener != null){
                    mListener.onYesClick();
                }
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: no");
                dismiss();
                if(mListener != null){
                    mListener.onNoClick();
                }
            }
        });
    }

    public interface OnCommonDialogListener {
        void onYesClick();
        void onNoClick();
    }
}
