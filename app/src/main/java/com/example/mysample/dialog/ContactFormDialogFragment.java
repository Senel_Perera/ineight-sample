package com.example.mysample.dialog;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.mysample.R;
import com.example.mysample.helper.Validator;
import com.example.mysample.object.User;
import com.example.mysample.widget.MontEditTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ContactFormDialogFragment extends DialogFragment {

    private static final String TAG = ContactFormDialogFragment.class.getSimpleName();

    private OnAddContactDialogListener mListener;
    private User user;
    protected Unbinder unbinder;
    private List<MontEditTextView> editTextViews = new ArrayList<>();

    @BindView(R.id.etFirstName)
    MontEditTextView etFirstName;
    @BindView(R.id.etLastName)
    MontEditTextView etLastName;
    @BindView(R.id.etCompanyName)
    MontEditTextView etCompanyName;
    @BindView(R.id.etEmailAddress)
    MontEditTextView etEmailAddress;
    @BindView(R.id.etAddress)
    MontEditTextView etAddress;
    @BindView(R.id.etMobileNumber)
    MontEditTextView etMobileNumber;
    @BindView(R.id.etUsername)
    MontEditTextView etUsername;
    @BindView(R.id.btnAddContact)
    Button btnAddContact;
    @BindView(R.id.btnClearForm)
    Button btnClearForm;

    public ContactFormDialogFragment() {
        // Required empty public constructor
    }

    public void setListener(OnAddContactDialogListener mListener) {
        this.mListener = mListener;
    }


    public static ContactFormDialogFragment newInstance() {
        ContactFormDialogFragment fragment = new ContactFormDialogFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        int width = getResources().getDimensionPixelSize(R.dimen.dialog_width);
        int height = getResources().getDimensionPixelSize(R.dimen.dialog_height);
        getDialog().getWindow().setLayout(width, height);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        View view = inflater.inflate(R.layout.fragment_add_contact_dialog, container, false);
        unbinder = ButterKnife.bind(this, view);
        addToEditTextList();

        btnAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Add");
                if(mListener != null){
                    if(!Validator.isEmptyFields(editTextViews) && Validator.isEmail(etEmailAddress)
                            && Validator.isValidPhoneNo(etMobileNumber)){
                        mListener.onAddContactsClick(getContactFormData());
                        getDialog().dismiss();
                    }
                }
            }
        });

        btnClearForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Clear");
                clearContactForm();
                if(mListener != null) {
                    mListener.onClearFormClick();
                }
            }
        });
        
        return view;
    }

    private void addToEditTextList() {
        editTextViews.add(etUsername);
        editTextViews.add(etFirstName);
        editTextViews.add(etLastName);
        editTextViews.add(etCompanyName);
        editTextViews.add(etEmailAddress);
        editTextViews.add(etMobileNumber);
        editTextViews.add(etAddress);
    }

    private void clearContactForm() {
        etFirstName.getText().clear();
        etLastName.getText().clear();
        etCompanyName.getText().clear();
        etEmailAddress.getText().clear();
        etAddress.getText().clear();
        etMobileNumber.getText().clear();
        etUsername.getText().clear();
    }

    private User getContactFormData() {
        user = new User();
        user.setFirstName(getEditTextData(etFirstName).trim());
        user.setLastName(getEditTextData(etLastName).trim());
        user.setCompanyName(getEditTextData(etCompanyName).trim());
        user.setEmailAddress(getEditTextData(etEmailAddress).trim());
        user.setAddress(getEditTextData(etAddress).trim());
        user.setMobileNumber(getEditTextData(etMobileNumber).trim());
        user.setUsername(getEditTextData(etUsername).trim());
        user.setCompanyId(getEditTextData(etCompanyName).trim() + user.getUserId());
        user.setUniqueId(user.getCompanyId()+user.getUserId());
        user.setIsOfflineData(true);
        user.setActive(true);
        return user;
    }

    private String getEditTextData(EditText editText) {
        String ediTextData;
        ediTextData = editText.getText().toString().trim();
        return ediTextData;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(unbinder != null)unbinder.unbind();
    }

    public interface OnAddContactDialogListener {
        void onAddContactsClick(User user);
        void onClearFormClick();
    }
}
