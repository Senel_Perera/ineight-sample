package com.example.mysample.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.example.mysample.ActivityManager;
import com.example.mysample.BaseActivity;
import com.example.mysample.R;
import com.example.mysample.SessionManager;
import com.example.mysample.helper.Helper;
import com.example.mysample.helper.Validator;
import com.example.mysample.object.ApiMessage;
import com.example.mysample.object.request.LoginRequest;
import com.example.mysample.object.response.LoginResponseEvent;
import com.example.mysample.widget.MontEditTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.mysample.service.RequestType.getAllContacts;
import static com.example.mysample.service.RequestType.login;

public class LoginActivity extends BaseActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private List<MontEditTextView> montEditTextViews = new ArrayList<>();
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.etUserId)
    MontEditTextView etUserId;
    @BindView(R.id.etPassword)
    MontEditTextView etPassword;
    @BindView(R.id.etCompanyId)
    MontEditTextView etCompanyId;
    @BindView(R.id.etProjectNumber)
    MontEditTextView etProjectNumber;

    SessionManager mSessionManager;

    @OnClick(R.id.btnLogin)
    public void onLoginPressed(){
        if(!Validator.isEmptyFields(montEditTextViews)) {
            showWaiting(this);
            requestLogin();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        mSessionManager = SessionManager.getInstance(this);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        //TODO remove checkAlreadyLogged it must be checked in the splash activity only
        checkAlreadyLogged();
        addEditTextsToList();
    }

    private void addEditTextsToList() {
        montEditTextViews.add(etUserId);
        montEditTextViews.add(etPassword);
        montEditTextViews.add(etCompanyId);
        montEditTextViews.add(etProjectNumber);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(LoginActivity.this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(LoginActivity.this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LoginResponseEvent event){
        dismissWaiting();
        Log.d(TAG, "onEvent: LoginResponseEvent " + event.toString());
        if(event.isSuccess() && event.getUserSessionKey() != null){
            if(event.getUserSessionKey().contains(getResources().getString(R.string.error))){
                mSessionManager.setSessionId(this,null);
                mSessionManager.setUserLogged(this,false);
                showCustomDialog(this, event.getUserSessionKey(),true, getResources().getString(R.string.ok));
            } else  {
                mSessionManager.setSessionId(this,event.getUserSessionKey());
                mSessionManager.setUserLogged(this,true);
                Log.d(TAG, "onEvent: " + event.getUserSessionKey());
                ActivityManager.startActivity(this, MainActivity.class);
                finish();
            }

        } else {
            showCustomDialog(this, getResources().getString(R.string.internal_server_error),true, getResources().getString(R.string.ok));
        }
    }

    private void requestLogin() {
        Log.d(TAG, "requestLogin: ");
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.userId = etUserId.getText().toString().trim();
        loginRequest.companyId = etCompanyId.getText().toString().trim();
        loginRequest.password = etPassword.getText().toString().trim();
        loginRequest.projectNumber = etProjectNumber.getText().toString().trim();
        ApiMessage event = new ApiMessage(login, loginRequest);
        EventBus.getDefault().post(event);
    }

    private void checkAlreadyLogged() {
        if (mSessionManager.isLoggedIn()){
            ActivityManager.startActivity(this, MainActivity.class);
        } else {
            Log.d(TAG, "checkAlreadyLogged: Not logged IN");
        }
    }
}
