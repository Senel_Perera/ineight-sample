package com.example.mysample.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.Toast;

import com.example.mysample.BaseActivity;
import com.example.mysample.R;
import com.example.mysample.helper.Helper;
import com.pspdfkit.annotations.Annotation;
import com.pspdfkit.annotations.LinkAnnotation;
import com.pspdfkit.annotations.StampAnnotation;
import com.pspdfkit.annotations.actions.Action;
import com.pspdfkit.annotations.actions.ActionType;
import com.pspdfkit.annotations.actions.GoToAction;
import com.pspdfkit.annotations.actions.UriAction;
import com.pspdfkit.configuration.PdfConfiguration;
import com.pspdfkit.document.PdfDocument;
import com.pspdfkit.document.formatters.DocumentJsonFormatter;
import com.pspdfkit.document.providers.DataProvider;
import com.pspdfkit.document.providers.InputStreamDataProvider;
import com.pspdfkit.listeners.DocumentListener;
import com.pspdfkit.listeners.OnDocumentLongPressListener;
import com.pspdfkit.ui.PdfFragment;
import com.pspdfkit.ui.PdfOutlineView;
import com.pspdfkit.ui.PdfThumbnailBar;
import com.pspdfkit.ui.PdfThumbnailGrid;
import com.pspdfkit.ui.search.PdfSearchViewModular;
import com.pspdfkit.ui.search.SearchResultHighlighter;
import com.pspdfkit.ui.special_mode.manager.TextSelectionManager;
import com.pspdfkit.utils.Size;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PDFActivity extends BaseActivity implements DocumentListener, OnDocumentLongPressListener {
    public static final String KEY_URI = "keyuri";
    public static final String SAMPLE_JSON = "document.json";
    public static final String SAMPLE_PDF_DIR = "file:///android_asset/sample.pdf";
    private static final String TAG = PDFActivity.class.getSimpleName();
    private static final int REQUEST_FOR_EXPORT_PERMISSION = 1;
    private static final int REQUEST_FOR_IMPORT_PERMISSION = 2;
    private PdfFragment fragment;
    private PdfThumbnailBar thumbnailBar;
    private PdfConfiguration configuration;
    private PdfSearchViewModular pdfSearchViewModular;
    private PdfThumbnailGrid pdfThumbnailGrid;
    private SearchResultHighlighter highlighter;
    private PdfOutlineView pdfOutlineView;
    private static final File documentJsonFile = new File(Environment.getExternalStorageDirectory(), SAMPLE_JSON);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        setContentView(R.layout.activity_pdf);
        initializePDF();
    }

    private void initializePDF() {
        configuration = new PdfConfiguration.Builder().build();
        fragment = (PdfFragment) getSupportFragmentManager(). findFragmentById(R.id.fragmentPdfContainer);
        if(fragment == null) {
            fragment = PdfFragment.newInstance(Uri.parse(SAMPLE_PDF_DIR), configuration);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentPdfContainer, fragment)
                    .commit();
        }
        fragment.addDocumentListener(this);
        fragment.setOnDocumentLongPressListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.export_import_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        boolean handled = false;
        final int itemId = item.getItemId();
        if(itemId == R.id.import_json) {
            handled = true;
            importDocumentJson(true);
        } else if (itemId == R.id.export_json) {
            handled = true;
            exportDocumentJson(true);
        }
        return handled || super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPageClick(@NonNull PdfDocument document,
                               int pageIndex,
                               @Nullable MotionEvent event,
                               @Nullable PointF pagePosition,
                               @Nullable Annotation clickedAnnotation) {
        Log.d(TAG, "onPageClick: pageIndex - " + pageIndex + "pagePosition - " + pagePosition);
        createStamp(pagePosition.x, pagePosition.y);
        createLinkAnnotation(pagePosition.x, pagePosition.y);
        return false;
    }

    @Override
    public boolean onDocumentLongPress(@NonNull PdfDocument pdfDocument,
                                       int pageIndex,
                                       @Nullable MotionEvent motionEvent,
                                       @Nullable PointF pointF,
                                       @Nullable Annotation annotation) {
        Log.d(TAG, "onDocumentLongPress: ");
        if(fragment.getView() != null) {
            fragment.getView().performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
        }
        if(annotation instanceof LinkAnnotation) {
            final Action action = ((LinkAnnotation) annotation).getAction();
            if(action != null && action.getType() == ActionType.URI) {
                String uri = ((UriAction) action).getUri();
                Log.d(TAG, "onDocumentLongPress: URI " + uri );
                Helper.showToastMessage(this, uri);
                return true;
            }
        }
        return false;
    }

    private void createStamp(float x, float y) {
        final PdfDocument document = fragment.getDocument();
        if(document == null) return;
        final int pageIndex = fragment.getPageIndex();
        final RectF rect = new RectF(
                x-30, y+30, x+ 30, y-30);

        final StampAnnotation stampAnnotation = new StampAnnotation(pageIndex, rect, "Highlighted");

        try {
            final Bitmap bitmap = BitmapFactory.decodeStream(getAssets().open("images/pin.png"));
            stampAnnotation.setBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "createStamp: " + e.getMessage());
        }
        final int color = Color.rgb(255, 0, 0);
        stampAnnotation.setColor(color);
        stampAnnotation.setFillColor(Color.rgb(255, 255, 255));
        addAnnotationToDocument(stampAnnotation);
    }

    private void createLinkAnnotation(float x, float y) {
        final int pageIndex = fragment.getPageIndex();
        final LinkAnnotation linkAnnotation = new LinkAnnotation(pageIndex);
        final PdfDocument document = fragment.getDocument();
        if(document == null) return;
        final Size pageSize = document.getPageSize(pageIndex);
        final RectF rect = new RectF(
                x-30, y+30, x+ 30, y-30);
        linkAnnotation.setBoundingBox(rect);
        GoToAction goToAction = new GoToAction(1);
        linkAnnotation.setAction(goToAction);
        fragment.addAnnotationToPage(linkAnnotation, false);
    }

    private void addAnnotationToDocument(StampAnnotation stampAnnotation) {
        fragment.addAnnotationToPage(stampAnnotation, false);
    }

    private static void printFileContentsToLogcat(final File file) {
        Log.d(TAG, "printFileContentsToLogcat: ");
        try (InputStream is = new FileInputStream(file)) {
            final BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            do {
                line = reader.readLine();
                if(line != null) Log.d(TAG, "printFileContentsToLogcat: " + line);
            } while (line != null);
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "printFileContentsToLogcat: " + e.getMessage());
        }
    }

    @SuppressLint("CheckResult")
    private void exportDocumentJson(final boolean requestExternalStorageAccess) {
        if(Helper.hasExternalStorageRwPermission(this)) {
            if(documentJsonFile.exists() && !documentJsonFile.delete()) {
                Toast.makeText(this, "Error while removing existing document.json prior to export.", Toast.LENGTH_LONG).show();
                return;
            }

            final OutputStream outputStream;
            try {
                outputStream = new FileOutputStream(documentJsonFile);
            } catch (FileNotFoundException e) {
                Toast.makeText(this, "Error while opening file 'document.json' for export. See logcat for more info.", Toast.LENGTH_LONG).show();
                Log.e(TAG, "Error while opening file 'document.json' for export", e);
                return;
            }

            DocumentJsonFormatter.exportDocumentJsonAsync(fragment.getDocument(), outputStream)
                    .subscribeOn(Schedulers.io())
                    .doOnComplete(() -> printFileContentsToLogcat(documentJsonFile))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            () -> Toast.makeText(PDFActivity.this, "Export successful! See /sdcard/document.json or logcat.", Toast.LENGTH_LONG).show(),
                            throwable -> {
                                Toast.makeText(PDFActivity.this, "Error while exporting document JSON. See logcat for more info.", Toast.LENGTH_LONG)
                                        .show();
                                Log.e(TAG, "Error while exporting document JSON", throwable);
                            }
                    );
        } else if (requestExternalStorageAccess) {
            Helper.requestExternalStorageRwPermission(this, REQUEST_FOR_EXPORT_PERMISSION);
        }
    }

    @SuppressLint("CheckResult")
    private void importDocumentJson(final boolean requestExternalStorageAccess) {
        if (Helper.hasExternalStorageRwPermission(this)) {
            if (!documentJsonFile.exists()) {
                Toast.makeText(this, "There's no document.json file on the external storage.", Toast.LENGTH_LONG).show();
                return;
            }

            final DataProvider inputProvider = new DocumentJsonDataProvider();
            DocumentJsonFormatter.importDocumentJsonAsync(fragment.getDocument(), inputProvider)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> Toast.makeText(this, "Import successful!", Toast.LENGTH_LONG).show(), throwable -> {
                        Toast.makeText(this, "Error while importing document JSON. See logcat for more info.", Toast.LENGTH_LONG)
                                .show();
                        Log.e(TAG, "Error while importing document JSON", throwable);
                    });
            Log.d(TAG, "importDocumentJson: " + inputProvider.getSize());
        } else if (requestExternalStorageAccess) {
            Helper.requestExternalStorageRwPermission(this, REQUEST_FOR_IMPORT_PERMISSION);
        }

    }

    private static class DocumentJsonDataProvider extends InputStreamDataProvider {
        @NonNull
        @Override
        protected InputStream openInputStream() throws Exception {
            return new FileInputStream(documentJsonFile);
        }

        @Override
        public long getSize() {
            return documentJsonFile.length();
        }

        @NonNull
        @Override
        public String getUid() {
            return "document-json";
        }

        @Nullable
        @Override
        public String getTitle() {
            return null;
        }
    }

}
