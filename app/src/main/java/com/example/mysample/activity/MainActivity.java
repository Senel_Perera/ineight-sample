package com.example.mysample.activity;

import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import com.example.mysample.ActivityManager;
import com.example.mysample.AppConstant;
import com.example.mysample.BaseActivity;
import com.example.mysample.R;
import com.example.mysample.SessionManager;
import com.example.mysample.database.ContactsDataSource;
import com.example.mysample.dialog.ContactFormDialogFragment;
import com.example.mysample.fragment.ContactMoreInfoFragment;
import com.example.mysample.fragment.ContactsFragment;
import com.example.mysample.fragment.DashboardFragment;
import com.example.mysample.helper.Helper;
import com.example.mysample.object.ApiMessage;
import com.example.mysample.object.User;

import com.example.mysample.object.UserContacts;
import com.example.mysample.object.request.ContactListRequest;
import com.example.mysample.receivers.NetworkStateReceiver;
import com.example.mysample.service.RequestType;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.fragment.app.Fragment;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.mysample.service.RequestType.getAllContacts;

public class MainActivity extends BaseActivity implements
        ContactsFragment.OnFragmentInteractionListener
        , DashboardFragment.OnFragmentInteractionListener
        , ContactFormDialogFragment.OnAddContactDialogListener
        , ContactMoreInfoFragment.OnFragmentInteractionListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String KEY_ADDRESS = "key_address";
    private static final String KEY_DASHBOARD = "key_dashboard";
    private static final String KEY_CONTACT_MORE_INFO = "key_contact_more_info";
    private static final String TAG_CONTACTS_FORM_DIALOG = "tag_country_form_dialog";

    private static String CURRENT_TAG = KEY_ADDRESS;
    private String[] screenTitles;
    public static int navItemIndex = 0;
    private Fragment mFragment;

    private View navHeader;
    private Handler mHandler;
    private NetworkStateReceiver networkStateReceiver;
    private IntentFilter intentFilter;
    private SessionManager mSessionManager;

    private ContactsDataSource contactsDataSource;
    private ContactListRequest mContactList;
    private List<User> userList = new ArrayList<>();

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.fab)
    FloatingActionButton mFab;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.nav_view)
    NavigationView mNavigationView;

    @OnClick(R.id.fab)
    public void onFabPressed(){
        initializeFormDialog();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        initializeActivity();
        EventBus.getDefault().register(this);
        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = KEY_ADDRESS;
            initializeFragmentView();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkStateReceiver);
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFab.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mFab.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return false;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onSaveEditedContacts(User user) {
        Log.d(TAG, "onSaveEditedContacts: ");
        updateContactList(user);

    }

    @Override
    public void onApiCall(RequestType requestType, Object object) {
        //TODO make the api call here for testing sample list will be added
        switch (requestType){
            case getAllContacts:
                requestContacts(1);
                break;

            case deleteContact:
                deleteContact(object);
                break;
        }
    }

    @Override
    public void onContactClick(User user) {
        CURRENT_TAG = KEY_CONTACT_MORE_INFO;
        setScreenTitle(user.getFirstName() + " " + user.getLastName());
        mFragment = ContactMoreInfoFragment.newInstance(user);
        mFab.hide();
        startFragment(R.id.nav_host_fragment, mFragment, false, CURRENT_TAG);

    }

    @Override
    public void onLoadMore(int position) {
        requestContacts(position);
    }

    @Override
    public void onAddContactsClick(User user) {
        Log.d(TAG, "onAddContactsClick: ");
        this.contactsDataSource = new ContactsDataSource(this);
        this.contactsDataSource.open();
        this.contactsDataSource.createContact(user);
        this.contactsDataSource.close();
        //Update list view with new data after adding
        getContactListFromDatabase();
    }

    @Override
    public void onClearFormClick() {
        Log.d(TAG, "onClearFormClick: ");

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UserContacts event){
        dismissWaiting();
        Log.d(TAG, "onEvent: UserContactsEvent " + event.toString());
        if(event.isSuccess()){
            if(mFragment instanceof ContactsFragment && event.getUserList().size() > 0){
                ((ContactsFragment) mFragment).setContacts(event.getUserList(),false);
            }
        } else {
            if(event.getErrorMessages().size()!=0){
                Log.d(TAG, "onEvent: ERROR RETRIEVING USERCONTACTS LIST - " + event.getErrorMessages().get(0).getErroMessage());
                showCustomDialog(MainActivity.this, event.getErrorMessages().get(0).getErroMessage(), true, getResources().getString(R.string.ok));
            } else {
                Log.d(TAG, "onEvent: ERROR RETRIEVING USERCONTACTS LIST - " + getResources().getString(R.string.error) );
                showCustomDialog(MainActivity.this, getResources().getString(R.string.error), true, getResources().getString(R.string.ok));
            }

        }
    }

    private void initializeActivity() {
        Log.d(TAG, "initializeActivity: ");
        navHeader = mNavigationView.getHeaderView(0);
        mHandler = new Handler();
        screenTitles = getResources().getStringArray(R.array.nav_titles);
        networkStateReceiver = new NetworkStateReceiver();
        intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        mSessionManager = SessionManager.getInstance(this);
        registerReceiver(networkStateReceiver, intentFilter);
        mContactList = new ContactListRequest();
        requestContacts(1);
        setUpNavigationView();
    }

    private void initializeFormDialog() {
        FragmentTransaction fragmentTransaction = MainActivity.this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ContactFormDialogFragment contactFormDialogFragment = ContactFormDialogFragment
                .newInstance();
        contactFormDialogFragment.setListener(MainActivity.this);
        contactFormDialogFragment.show(fragmentTransaction, TAG_CONTACTS_FORM_DIALOG);
    }

    private void initializeFragmentView() {
        Log.d(TAG, "initializeFragmentView: ");
        selectNavItem();
        setScreenTitle(screenTitles[navItemIndex]);
        //check whether the fragment is already opened/displayed or in the stack
        if(checkInstance(CURRENT_TAG))return;

        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                mFragment = getSelectedFragment();
                startFragment(R.id.nav_host_fragment, mFragment, false, CURRENT_TAG);
            }
        };

        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        mDrawerLayout.closeDrawers();
        invalidateOptionsMenu();
    }

    private void setUpNavigationView() {
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Log.d(TAG, "onNavigationItemSelected: " + menuItem.getItemId());
                mFab.show();
                switch (menuItem.getItemId()) {
                    case R.id.nav_address:
                        navItemIndex = 0;
                        CURRENT_TAG = KEY_ADDRESS;
                        break;

                    case R.id.nav_dashboard:
                        navItemIndex = 1;
                        CURRENT_TAG = KEY_DASHBOARD;
                        break;

                    case R.id.nav_pdf:
                        ActivityManager.startActivity(MainActivity.this, PDFActivity.class);
                        break;

                    case R.id.nav_logout:
                        logoutUser();
                        break;
                }

                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);
                initializeFragmentView();
                return true;
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout
                , mToolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        mDrawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    private void logoutUser() {
        mSessionManager.setUserLogged(this, false);
        ActivityManager.startActivity(this, LoginActivity.class);
        finish();
    }

    private void setScreenTitle(String titleName) {
        Log.d(TAG, "setScreenTitle: " + titleName);
        getSupportActionBar().setTitle(titleName);
    }

    private void requestContacts(int startRow) {
        if(Helper.isNetworkAvailable(MainActivity.this)) {
            showWaiting(MainActivity.this);
            mContactList.sessionKey = mSessionManager.getSessionId();
            mContactList.startRowPosition = String.valueOf(startRow);
            mContactList.noOfRows = AppConstant.LIST_NO_OF_ROWS;
            ApiMessage event = new ApiMessage(getAllContacts, mContactList);
            EventBus.getDefault().post(event);
        } else {
            getContactListFromDatabase();
        }
    }

    private Fragment getSelectedFragment() {
        Log.d(TAG, "getSelectedFragment: " + navItemIndex);
        switch (navItemIndex) {
            case 0:
                ContactsFragment contactsFragment = ContactsFragment.newInstance();
                return contactsFragment;
            case 1:
                DashboardFragment dashboardFragment = DashboardFragment.newInstance();
                return dashboardFragment;
        }
        return null;
    }

    private void selectNavItem() {
        mNavigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void getContactListFromDatabase() {
        Log.d(TAG, "getContactListFromDatabase: ");
        this.contactsDataSource = new ContactsDataSource(this);
        this.contactsDataSource.open();
        this.userList = this.contactsDataSource.getContactList();
        this.contactsDataSource.close();
        ((ContactsFragment) mFragment).setContacts(userList,true);
    }

    private void deleteContact(Object object) {
        Log.d(TAG, "deleteContactById: ");
        User user = (User) object;
        this.contactsDataSource = new ContactsDataSource(this);
        this.contactsDataSource.open();
        Log.d(TAG, "deleteContactById: user: " + user.getUserId());
        this.contactsDataSource.deleteContact(user);
        this.contactsDataSource.close();
        //Update list view with new data after adding
        getContactListFromDatabase();
    }

    private void updateContactList(User user) {
        Log.d(TAG, "updateContactList: ");
        this.contactsDataSource = new ContactsDataSource(this);
        this.contactsDataSource.open();
        this.contactsDataSource.updateContact(user);
        this.contactsDataSource.close();
        setScreenTitle(user.getFirstName() + " " + user.getLastName());
    }

}
