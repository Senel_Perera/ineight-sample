package com.example.mysample.service;

public enum RequestType {
    login,
    addContact,
    deleteContact,
    editContact,
    getAllContacts
}
