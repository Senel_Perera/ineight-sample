package com.example.mysample.service;

import android.content.Context;
import android.util.Log;

import com.example.mysample.AppConstant;
import com.example.mysample.object.ApiMessage;
import com.example.mysample.object.Error;
import com.example.mysample.object.User;
import com.example.mysample.object.UserContacts;
import com.example.mysample.object.request.ContactListRequest;
import com.example.mysample.object.request.LoginRequest;
import com.example.mysample.object.response.ContactResponseEvent;
import com.example.mysample.object.response.ContactData;
import com.example.mysample.object.response.LoginResponseEvent;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Type;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Controller {
    private static final String TAG = Controller.class.getSimpleName();
    private static final String SUCCESS = "success";
    private static final String ERROR = "error";
    private InEightDocumentAPI mApi;
    private Gson gson;

    public Controller(Context context) {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        String BASE_URL = AppConstant.BASE_URL;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        mApi = retrofit.create(InEightDocumentAPI.class);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onEvent(ApiMessage event) {
        switch (event.getType()) {
            case login:
                loginUser(event.getObject()); //Logon API
                break;
            case getAllContacts:
                getContactList(event.getObject()); //ListContactsData API
                break;
        }
    }

    private void loginUser(Object object) {
        Log.d(TAG, "loginUser: ");
        mApi.login((LoginRequest) object).enqueue(new Callback<LoginResponseEvent>() {
            @Override
            public void onResponse(Call<LoginResponseEvent> call, Response<LoginResponseEvent> response) {
                Log.d(TAG, "[onResponse]:-> " + response.body());
                LoginResponseEvent res;
                if(response.isSuccessful()){
                    res = response.body();
                    Log.d(TAG, "onResponse---------> success" + res.toString());
                    res.setCode(1);
                    res.setCustomMessage(SUCCESS);
                    res.setSuccess(true);
                } else {
                    Log.d(TAG, "onResponse----------> fail");
                    res = new LoginResponseEvent();
                    res.setCode(0);
                    res.setCustomMessage(ERROR);
                    res.setSuccess(false);
                }
                EventBus.getDefault().post(res);
            }

            @Override
            public void onFailure(Call<LoginResponseEvent> call, Throwable t) {
                Log.d(TAG, "onFailure: ------------>");
                LoginResponseEvent responseEvent = new LoginResponseEvent();
                responseEvent.setCode(0);
                responseEvent.setCustomMessage(t.getMessage());
                responseEvent.setSuccess(false);
                EventBus.getDefault().post(responseEvent);
            }
        });


    }

    public void getContactList(Object object) {
        Log.d(TAG, "getContactList: ");
        UserContacts event = new UserContacts();
        mApi.getContacts((ContactListRequest) object).enqueue(new Callback<ContactResponseEvent>() {
            @Override
            public void onResponse(Call<ContactResponseEvent> call, Response<ContactResponseEvent> response) {
                Log.d(TAG, "[onResponse]:-> " + response);
                ContactData res = new ContactData();
                if(response.isSuccessful()) {
                    Log.d(TAG, "onResponse-----------> isSuccessful" + res.toString());
                    res = response.body().contactData;
                    gson = new Gson();
                    Type listType = new TypeToken<List<User>>(){}.getType();
                    Type errorListType = new TypeToken<List<Error>>(){}.getType();
                    if(res.getMessage()!=null){
                        Log.d(TAG, "onResponse-----------> fail" + res.toString());
                        event.setCode(0);
                        event.setErrorMessages(gson.fromJson(res.getMessage(),errorListType));
                        event.setSuccess(false);
                    } else {
                        Log.d(TAG, "onResponse-----------> success" + res.toString());
                        event.setUserList(gson.fromJson(res.contactListJsonObject,listType));
                        event.setCode(1);
                        event.setCustomMessage(SUCCESS);
                        event.setSuccess(true);
                    }
                }
                Log.d(TAG, "onResponse: Converting to JSON OBJECT " + event.toString());
                EventBus.getDefault().post(event);
            }

            @Override
            public void onFailure(Call<ContactResponseEvent> call, Throwable t) {
                Log.d(TAG, "onFailure---------->: " + t.getMessage());
                event.setCode(0);
                event.setCustomMessage(t.getMessage());
                event.setSuccess(false);
            }
        });
    }
}
