package com.example.mysample.service;

import com.example.mysample.object.request.ContactListRequest;
import com.example.mysample.object.request.LoginRequest;
import com.example.mysample.object.response.ContactResponseEvent;
import com.example.mysample.object.response.LoginResponseEvent;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface InEightDocumentAPI {

    @POST("adrbook.asmx/ListContactsData")
    Call<ContactResponseEvent> getContacts(@Body ContactListRequest contactListRequest);

    @POST("session.asmx/Logon")
    Call<LoginResponseEvent> login(@Body LoginRequest loginRequest);
}
