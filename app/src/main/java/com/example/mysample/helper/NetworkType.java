package com.example.mysample.helper;

public enum NetworkType {
    WIFI, MOBILE, NONE
}
