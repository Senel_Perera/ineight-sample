package com.example.mysample.helper;

import android.util.Patterns;

import com.example.mysample.R;
import com.example.mysample.widget.MontEditTextView;

import java.util.List;

public class Validator {

    public static Boolean isEmpty(MontEditTextView editTextView) {
        String text = editTextView.getText().toString().trim();
        if(!text.isEmpty()) {
            return false;
        } else {
//            setError(editTextView, editTextView.getContext().getString(R.string.error_required_field));
            return true;
        }
    }

    public static Boolean isEmail(MontEditTextView editTextView) {
        if(isEmpty(editTextView))return false;
        String text = editTextView.getText().toString();
        if(Patterns.EMAIL_ADDRESS.matcher(text).matches()){
            return true;
        } else {
            setError(editTextView, editTextView.getContext().getString(R.string.error_invalid_email));
            return false;
        }
    }

    public static boolean isValidPhoneNo(MontEditTextView editTextView) {
        if(isEmpty(editTextView))return false;
        String text = editTextView.getText().toString();
        if(Patterns.PHONE.matcher(text).matches()){
            return true;
        } else {
            setError(editTextView, editTextView.getContext().getString(R.string.error_invalid_phone));
            return false;
        }
    }

    public static boolean isEmptyFields(List<MontEditTextView>editTextViews){
        for(MontEditTextView editTextView : editTextViews){
            if(isEmpty(editTextView)){
                Helper.showToastMessage(ContextManager.getContext()
                        ,ContextManager.getContext().getResources().getString(R.string.error_all_fields_required));
                setError(editTextView, editTextView.getContext().getString(R.string.error_required_field));
                return true;
            }
        }
     return false;
    }

    public static void setError(MontEditTextView editTextView, String errorMessage) {
        if(editTextView == null || errorMessage == null) return;
        editTextView.setError(errorMessage);
    }
}
