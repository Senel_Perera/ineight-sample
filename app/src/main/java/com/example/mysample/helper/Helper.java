package com.example.mysample.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.mysample.R;
import com.example.mysample.widget.CustomFormRow;
import com.example.mysample.widget.MontEditTextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Arrays;
import java.util.List;

public class Helper {
    public static final String TAG = Helper.class.getSimpleName();

    public static void toggleEditableFormFields(List<CustomFormRow> customFormRows, Boolean isEditable){
        Log.d(TAG, "toggleEditableFormFields: " + customFormRows.size());
        for(CustomFormRow customFormRow : customFormRows){
            customFormRow.setIsEditable(isEditable);
        }
    }

    public static boolean isCustomEditsEmpty(List<CustomFormRow> customFormRows) {
        Log.d(TAG, "isCustomEditsEmpty: " + customFormRows.size());
        for(CustomFormRow customFormRow : customFormRows) {
            if(Validator.isEmpty(customFormRow.getEditText())){
                Helper.showToastMessage(ContextManager.getContext()
                        ,ContextManager.getContext().getResources().getString(R.string.error_all_fields_required));
                Validator.setError(customFormRow.getEditText(), customFormRow.getEditText().getContext().getString(R.string.error_required_field));
                return true;
            }
        }
        return false;
    }

    public static void editTextEditCheck(MontEditTextView editTextView, Boolean flag, Boolean def) {
        if(flag==null){
            flag = def;
        }
        editTextView.setFocusable(flag);
        editTextView.setEnabled(flag);
        editTextView.setFocusableInTouchMode(flag);
        editTextView.setClickable(flag);

    }

    public static void showToastMessage(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static <T> List<T> convertArrayToList(T array[])
    {
        List<T> list = Arrays.asList(array);
        return list;
    }

    public static NetworkType getNetworkType(Context context) {
        NetworkType type;
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo == null) return NetworkType.NONE;
        switch (activeNetworkInfo.getType()) {
            case ConnectivityManager.TYPE_MOBILE:
                type = NetworkType.MOBILE;
                break;
            case ConnectivityManager.TYPE_WIFI:
                type = NetworkType.WIFI;
                break;
            default:
                type = NetworkType.NONE;
                break;
        }
        return type;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static <T> List<T> convertJsonStringToList(Class<T> t, String jsonString){
        Gson gson = new Gson();
        TypeToken<List<T>> token = new TypeToken<List<T>>() {};
        List<T> list = gson.fromJson(jsonString, token.getType());
//        Type listType = new TypeToken<List<T>>(){};
        Log.d(TAG, "convertJsonStringToList: " + list.toString());
        return list;
    }

    public static boolean hasExternalStorageRwPermission(Context context) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M
                || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean requestExternalStorageRwPermission(Activity activity, int requestCode) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                        activity,
                        new String[] {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        requestCode
                );
                return false;
            }
        }
        return true;
    }
}
