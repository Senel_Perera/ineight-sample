package com.example.mysample.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mysample.R;
import com.example.mysample.object.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> implements Filterable {
    private static final String TAG = AddressAdapter.class.getSimpleName();
    private AddressAdapterCallback mCallback;
    private List<User> mUserFiltered = new ArrayList<>();
    private List<User> mUser = new ArrayList<>();
    public View mForeground;

    public AddressAdapter(List<User> userObject, AddressAdapterCallback callback){
        mCallback = callback;
        mUser.addAll(userObject);
        mUserFiltered.addAll(userObject);
    }

    public void add(int position, User user){
        mUserFiltered.add(position, user);
        notifyDataSetChanged();
    }

    public void addAll(List<User> users, Boolean isClear){
        if(isClear){
            mUserFiltered.clear();
        }
        mUserFiltered.addAll(users);
        mUser.addAll(users);
        notifyDataSetChanged();
    }

    public void remove(int position){

    }

    public void refresh(){
        notifyDataSetChanged();
    }

    public User getUser(int position){
        return mUserFiltered.get(position);
    }

    public void reloadUserData() {
        mUserFiltered = mUser;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        int position = 0;
        User user;
        @BindView(R.id.tvFirstName)
        TextView tvFirstName;
        @BindView(R.id.tvLastName)
        TextView tvLastName;
        @BindView(R.id.tvCompanyName)
        TextView tvCompanyName;
        @BindView(R.id.tvEmail)
        TextView tvEmail;
        @BindView(R.id.tvMobile)
        TextView tvMobile;
        @BindView(R.id.ivStatus)
        LinearLayout lvStatusIndicator;
        public LinearLayout lnForeground;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            lnForeground = itemView.findViewById(R.id.lnForeground);
            ButterKnife.bind(this,itemView);
        }

        @Override
        public void onClick(View view) {
            Log.d(TAG, "onClick: ");
            mCallback.onItemClick(position, view, user);
        }

        @Override
        public boolean onLongClick(View view) {
            Log.d(TAG, "onLongClick: ");
            mCallback.onItemLongPress(position, view, user);
            return true;
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_address, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddressAdapter.ViewHolder holder, int position) {
        holder.position = holder.getAdapterPosition();
        holder.user = mUserFiltered.get(holder.position);
        holder.tvFirstName.setText(holder.user.getFirstName());
        holder.tvLastName.setText(holder.user.getLastName());
        holder.tvCompanyName.setText(holder.user.getCompanyName());
        holder.tvEmail.setText(holder.user.getEmailAddress());
        holder.tvMobile.setText(holder.user.getMobileNumber());
        mForeground = holder.lnForeground;
    }

    @Override
    public int getItemCount() {
        return mUserFiltered.size();
    }

    @Override
    public Filter getFilter() {
        Log.d(TAG, "getFilter: ");
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String searchText = charSequence.toString();
                if(searchText.isEmpty()){
                    mUserFiltered = mUser;
                } else {
                    List<User> filteredUserList = new ArrayList<>();
                    for (User user : mUser) {
                        if(user.getFirstName().toLowerCase().contains(searchText.toLowerCase())
                                || user.getLastName().toLowerCase().contains(searchText.toLowerCase())) {
                            filteredUserList.add(user);
                        }
                    }
                    mUserFiltered = filteredUserList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mUserFiltered;
                Log.d(TAG, "performFiltering: " + mUserFiltered.size());
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mUserFiltered = (ArrayList<User>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface AddressAdapterCallback {
        void onItemClick(int adapterPosition, View view, User user);
        void onItemLongPress(int adapterPosition, View view, User user);
    }
}
