package com.example.mysample.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.annotation.StyleableRes;

import com.example.mysample.R;
import com.example.mysample.helper.Helper;

public class CustomFormRow extends LinearLayout {

    @StyleableRes
    int formLabel = 0;
    @StyleableRes
    int formField = 1;
    @StyleableRes
    int formEdit = 2;

    MontTextView tvLabel;
    MontEditTextView etField;


    public CustomFormRow(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs);
    }

    public CustomFormRow(Context context) {
        super(context);
    }

    private void initialize(Context context, AttributeSet attrs) {
        inflate(context, R.layout.item_form_row, this);

        int[] sets = {R.attr.tvLabel, R.attr.etField, R.attr.isEditable};
        TypedArray typedArray = context.obtainStyledAttributes(attrs, sets);

        CharSequence label = typedArray.getText(formLabel);
        CharSequence field = typedArray.getText(formField);
        Boolean isEditable = typedArray.getBoolean(formEdit,false);
        typedArray.recycle();

        initializeComponents();

        setFieldText(field);
        setLabelText(label);
        setIsEditable(isEditable);
    }

    private void initializeComponents() {
        tvLabel = (MontTextView) findViewById(R.id.tvLabel);
        etField = (MontEditTextView) findViewById(R.id.etField);
    }

    public CharSequence getLabelText() {
        return tvLabel.getText();
    }

    public void setLabelText(CharSequence value) {
        tvLabel.setText(value);
    }

    public CharSequence getFieldText() {
        return etField.getText();
    }

    public void setFieldText (CharSequence value) {
        etField.setText(value);
    }

    public void setIsEditable (Boolean flag) {
        Helper.editTextEditCheck(etField, flag, false);
    }

    public MontEditTextView getEditText() {
        return etField;
    }
}
