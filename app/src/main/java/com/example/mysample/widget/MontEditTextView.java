package com.example.mysample.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.example.mysample.R;

public class MontEditTextView extends AppCompatEditText {
    private static final String TAG = MontTextView.class.getSimpleName();
    String customFont;

    public MontEditTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    public MontEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context context, AttributeSet attrs) {
//        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Light.ttf"));
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        int customFontTextView = typedArray.getInteger(R.styleable.CustomFontTextView_fontName, 0);

        switch (customFontTextView) {
            case 1:
                customFont = getResources().getString(R.string.Montserrat_Light);
                break;

            case 2:
                customFont = getResources().getString(R.string.Montserrat_Medium);
                break;

            case 3:
                customFont = getResources().getString(R.string.Montserrat_Regular);
                break;

            case 4:
                customFont = getResources().getString(R.string.Montserrat_SemiBold);
                break;

            default:
                customFont = getResources().getString(R.string.Montserrat_Regular);
                break;
        }

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + customFont + ".ttf");
        setTypeface(typeface);
        typedArray.recycle();
    }
}
