package com.example.mysample;

import android.content.Context;
import android.view.View;

import androidx.fragment.app.Fragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BaseFragment extends Fragment {

    protected Unbinder unbinder;
    protected Context context;

    protected void init(Fragment fragment, View view) {
        context = view.getContext();
        unbinder = ButterKnife.bind(fragment, view);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(unbinder!=null)unbinder.unbind();
    }
}
