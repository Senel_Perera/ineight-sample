package com.example.mysample;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SessionManager {
    private static final String TAG = SessionManager.class.getSimpleName();
    private static SessionManager mInstance;
    private boolean mIsLoggedIn;
    private String mSessionId;

    public static SessionManager getInstance(Context context) {
        if(mInstance == null){
            mInstance = new SessionManager();
        }
        SharedPreferences preferences = context.getSharedPreferences(
                AppConstant.SharedPrefKeys.APP_PREFERENCES, Context.MODE_PRIVATE);
        mInstance.mIsLoggedIn = preferences.getBoolean(
                AppConstant.SharedPrefKeys.KEY_IS_LOGGED_IN, false);
        mInstance.mSessionId = preferences.getString(
                AppConstant.SharedPrefKeys.KEY_SESSION_ID, null);
        return mInstance;
    }

    public void setUserLogged(Context context, boolean logged) {
        Log.d(TAG, "setUserLogged: " + logged);
        if(mIsLoggedIn != logged) {
            SharedPreferences.Editor editor = context.getSharedPreferences(
                    AppConstant.SharedPrefKeys.APP_PREFERENCES, Context.MODE_PRIVATE)
                    .edit();
            editor.putBoolean(AppConstant.SharedPrefKeys.KEY_IS_LOGGED_IN, logged);
            editor.commit();
        }
        this.mIsLoggedIn = logged;
    }

    public Boolean isLoggedIn(){
        return mIsLoggedIn;
    }

    public void setSessionId(Context context, String sessionId) {
        Log.d(TAG, "sessionId: " + sessionId);
        SharedPreferences.Editor editor = context.getSharedPreferences(
                AppConstant.SharedPrefKeys.APP_PREFERENCES, Context.MODE_PRIVATE)
                .edit();
        editor.putString(AppConstant.SharedPrefKeys.KEY_SESSION_ID, sessionId);
        editor.commit();
        this.mSessionId = sessionId;
    }

    public String getSessionId(){
        return mSessionId;
    }
}
