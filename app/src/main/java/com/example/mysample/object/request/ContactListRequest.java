package com.example.mysample.object.request;

public class ContactListRequest {
    public String sessionKey="";
    public String contactFilter="";
    public String orderBy="";
    public String startRowPosition="";
    public String noOfRows="";
    public String companyIntKey="0";
    public String selectedUserSavedFilter="-1";
    public String quickSearchText="";
    public String customViewNo="0";
}
