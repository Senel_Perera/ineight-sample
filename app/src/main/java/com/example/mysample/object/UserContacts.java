package com.example.mysample.object;

import java.util.ArrayList;
import java.util.List;

public class UserContacts extends Message {

    private List<User> userList = new ArrayList<>();

    @Override
    public String toString() {
        return "UserContacts{" +
                "userList=" + userList +
                '}';
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

}
