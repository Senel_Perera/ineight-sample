package com.example.mysample.object.request;

import com.google.gson.annotations.SerializedName;

public class LoginRequest {
    @SerializedName("userID")
    public String userId;
    @SerializedName("companyID")
    public String companyId;
    @SerializedName("password")
    public String password;
    @SerializedName("projNo")
    public String projectNumber;
}
