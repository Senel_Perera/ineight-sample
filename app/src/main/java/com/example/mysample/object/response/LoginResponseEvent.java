package com.example.mysample.object.response;

import com.example.mysample.object.Message;
import com.google.gson.annotations.SerializedName;

public class LoginResponseEvent extends Message {
    @SerializedName("d")
    private String userSessionKey;

    @Override
    public String toString() {
        return "LoginResponseEvent{" +
                "userSessionKey='" + userSessionKey + '\'' +
                '}';
    }

    public String getUserSessionKey() {
        return userSessionKey;
    }

    public void setUserSessionKey(String userSessionKey) {
        this.userSessionKey = userSessionKey;
    }

}
