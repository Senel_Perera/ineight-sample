package com.example.mysample.object;

import java.util.ArrayList;
import java.util.List;

public class Message {

    private int code; //1 - success 0 fail
    private boolean isSuccess; // message
    private List<Error> errorMessages = new ArrayList<>();
    private String customMessage;

    @Override
    public String toString() {
        return "Message{" +
                "code=" + code +
                ", isSuccess=" + isSuccess +
                ", errorMessages=" + errorMessages +
                ", customMessage='" + customMessage + '\'' +
                '}';
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public List<Error> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(List<Error> errorMessages) {
        this.errorMessages = errorMessages;
    }

    public String getCustomMessage() {
        return customMessage;
    }

    public void setCustomMessage(String customMessage) {
        this.customMessage = customMessage;
    }
}
