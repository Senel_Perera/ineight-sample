package com.example.mysample.object;

import com.example.mysample.service.RequestType;

public class ApiMessage {

    private RequestType type;
    private Object object;

    @Override
    public String toString() {
        return "ApiMessage{" +
                "type=" + type +
                ", object=" + object +
                '}';
    }

    public ApiMessage(RequestType type, Object object) {
        this.type = type;
        this.object = object;
    }

    public RequestType getType() {
        return type;
    }

    public void setType(RequestType type) {
        this.type = type;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }


}
