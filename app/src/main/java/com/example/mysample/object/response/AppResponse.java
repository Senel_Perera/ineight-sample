package com.example.mysample.object.response;

import com.google.gson.annotations.SerializedName;

public class AppResponse {
    @SerializedName("fReturnValue")
    private String message; // message

    @Override
    public String toString() {
        return "AppResponse{" +
                "message='" + message + '\'' +
                '}';
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
