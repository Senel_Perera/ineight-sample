package com.example.mysample.object;

import com.google.gson.annotations.SerializedName;

public class Error {

    @SerializedName("ReturnValue")
    private String errorMessage;

    @Override
    public String toString() {
        return "Error{" +
                "errorMessage='" + errorMessage + '\'' +
                '}';
    }

    public String getErroMessage() {
        return errorMessage;
    }

    public void setErroMessage(String erroMessage) {
        this.errorMessage = erroMessage;
    }
}
