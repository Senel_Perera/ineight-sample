package com.example.mysample.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class User implements Parcelable {

    private long userId;
    @SerializedName("CompanyId")
    private String companyId;
    private String uniqueId;
    @SerializedName("FirstName")
    private String firstName;
    @SerializedName("LastName")
    private String lastName;
    @SerializedName("Company")
    private String companyName;
    @SerializedName("Email")
    private String emailAddress;
    @SerializedName("Address1")
    private String address;
    @SerializedName("PhoneMob")
    private String mobileNumber;
    @SerializedName("AdUserName")
    private String username;
    private boolean isActive;
    private boolean isOfflineData;

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ",companyId=" + companyId +
                ",uniqueId=" + uniqueId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", companyName='" + companyName + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", address='" + address + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", username='" + username + '\'' +
                ", isActive=" + isActive +
                ", isOfflineData=" + isOfflineData +
                '}';
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isOfflineData() {
        return isOfflineData;
    }

    public void setIsOfflineData(boolean offlineData) {
        isOfflineData = offlineData;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.userId);
        dest.writeString(this.companyId);
        dest.writeString(this.uniqueId);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.companyName);
        dest.writeString(this.emailAddress);
        dest.writeString(this.address);
        dest.writeString(this.mobileNumber);
        dest.writeString(this.username);
        dest.writeByte(this.isActive ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isOfflineData ? (byte) 1 : (byte) 0);
    }

    public User() {
    }

    protected User(Parcel in) {
        this.userId = in.readLong();
        this.companyId = in.readString();
        this.uniqueId = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.companyName = in.readString();
        this.emailAddress = in.readString();
        this.address = in.readString();
        this.mobileNumber = in.readString();
        this.username = in.readString();
        this.isActive = in.readByte() != 0;
        this.isOfflineData = in.readByte() != 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
