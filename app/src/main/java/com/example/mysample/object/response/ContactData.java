package com.example.mysample.object.response;

import com.google.gson.annotations.SerializedName;

public class ContactData extends AppResponse {
    @SerializedName("fContacts")
    public String contactListJsonObject;

    @Override
    public String toString() {
        return "ContactData{" +
                "contactListJsonObject='" + contactListJsonObject + '\'' +
                '}';
    }

}
