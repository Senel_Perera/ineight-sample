package com.example.mysample.object.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactResponseEvent {

    @SerializedName("d")
    @Expose
    public ContactData contactData;
}
